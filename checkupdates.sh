#!/bin/bash

set -ex

export PATH=/home/bubu/fdroidserver/:$PATH
readarray -t apps < applist

pushd fdroiddata
git pull
fdroid checkupdates --auto --autoonly --commit ${apps[@]}
git pull && git push
