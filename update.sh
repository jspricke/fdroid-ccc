#!/bin/bash

set -ex

export PATH=/home/bubu/fdroidserver/:$PATH
readarray -t apps < applist
pushd fdroiddata
commit=$(git rev-parse HEAD)
git pull
if [ $(git rev-parse HEAD) != ${commit} ]; then
    fdroid build ${apps[@]}
    fdroid publish
    fdroid update
    popd
    pushd gitrepo
    git add .
    git commit -m "automatic repo update"
    git push
    popd
fi
